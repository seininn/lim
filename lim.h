/*
    Lin (لِم): A bracket parser in a convenient package (.c/.h)

    Copyright (C) 2012  Sulaiman A. Mustafa
    
    [ Alternative licensing is available ]
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    
    Contact me if an alternative license is desired. Depending on the type of 
    intended use, an alternative license might be granted free of charge, in 
    exchange for compensation, or it might not be granted at all.
    
    Contact: http://people.sigh.asia/~sulaiman/about/contact/

*/

#define LIM_NOMEM -1

typedef struct plim_s{
    char *brackets[2];
    char *text;
    struct plim_s **list;
} lim_s;

extern int lim_errno;

lim_s** limParse(char *string, char **opining_brackets, char **closing_brackets, char **singletons, int *cur);
/*
    limParse: Parse a string and return a NULL-terminated list of lim_s nodes
    
    Arguments:
    
        string              The string that should be parsed.
       
        opining_brackets    A NULL-Terminated array of strings that contain any 
                            opening brackets that limParse should recognize. 
                            These strings need not be actual brackets, and any 
                            string of any non-zero length is allowed.
                            
        opining_brackets    A NULL-Terminated array of strings that contain any 
                            closing brackets that limParse should recognize. 
                            This array MUST be the same length of 
                            `opining_brackets` and elements must be positioned
                            to match their counterpart's position in 
                            `opining_brackets`.
                            
        singletons          A NULL-Terminated array of strings that contain any 
                            strings that limParse should recognize as delimiting
                            brackets. The first matching singleton on the string
                            after is taken to be the closing backet unless it is
                            contained in a substring surrounded by brackets 
                            present in `opining_brackets` and 
                            `closing_brackets`.
                            
        cur                 The address at which the index of the first 
                            unprocessed character is stored. This address is 
                            updated regardless of the success of this function
                            and is intended for reporting errors in syntaxual
                            errors in `string`.


    Return
    
        This function returns a NULL-terminated list of lim_s nodes that should
        be freed using `limFree` or NULL on error.


    Thread Safety
            
        lim_errno can be used for debugging purposes but is not designed to work
        in a multi-threaded environment or any environment where limParse can 
        be invoked concurrently.
        
        This function's last parameter, `cur` should be unique to each thread; 
        i.e. don't pass the same location from multiple cuncurrent threads.
       
*/
                    
void limFree(lim_s **list);
/*
    limFree: Frees the memory allocated for a lim_s list
    
    Argument
        
        list                A list returned by limParse
    
        
    Return NONE    
*/

void limPrint(lim_s **list, int indention);
/*
    limPrint: A convenience function that prints a lim_s list
    
    Arguments
    
        list                A list returned by limParse
        
        indention           The amount to indent for each sublist

    
    Return NONE
*/


