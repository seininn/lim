/*
    Lim (لِم): A bracket parser in a convenient package (.c/.h)

    Copyright (C) 2012  Sulaiman A. Mustafa
    
    [ Alternative licensing is available ]
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
    
    Contact me if an alternative license is desired. Depending on the type of 
    intended use, an alternative license might be granted free of charge, in 
    exchange for compensation, or it might not be granted at all.
    
    Contact: http://people.sigh.asia/~sulaiman/about/contact/
    
    ----------------------------------------------------------------------------
    TODO:
        limParseNode: parse the text in a TEXTNODE and turn it into a LISTNODE
                      
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lim.h"

#define ABORT_IF(X) if (X) {fprintf(stderr, "failed at line %d", __LINE__); abort();}
#define ABORT_IF_MISMATCH(X, Y) ABORT_IF(strcmp((X), (Y)))

int lim_errno;

char *limDup(char* string){
    char *p;
    if (!(p = malloc(strlen(string)+1))) {
        lim_errno = __LINE__;
        return NULL;
    }
    strcpy(p, string);
    return p;
}

char *limSub(char* string, int len){
    char *p;
    if (!(p = malloc(len+1))) {
        lim_errno = __LINE__;
        return NULL;
    }
    strncpy(p, string, len);
    p[len] = '\0';
    return p;
}

void limFree(lim_s **s){
    lim_s **l = s;
    if (!s) return;
    while (s[0]) {
        if (s[0]->brackets[0]) free(s[0]->brackets[0]);
        if (s[0]->brackets[1]) free(s[0]->brackets[1]);
        if (s[0]->text) free(s[0]->text);
        else limFree(s[0]->list);
        free(s[0]);
        ++s;
    }
    free(l);
}

lim_s* limNode(char *obrac, char *cbrac, char *string, int len) {
    /*
        len == 0; whole string
        len > 0 ; sub string
    */
    lim_s *node;
    
    if (!(node = malloc(sizeof(lim_s)))) {
        lim_errno = __LINE__;
        return NULL;
    }
    
    node->list = NULL;
    
    if (!obrac) node->brackets[0] = NULL;
    else if (!(node->brackets[0] = limDup(obrac))) {
        free(node);
        return NULL;
    } 
    
    if (!cbrac) node->brackets[1] = NULL;
    else if (!(node->brackets[1] = limDup(cbrac))) {
        if (node->brackets[0]) free(node->brackets[0]);
        free(node);
        return NULL;
    } 
    
    if (string) {
        if (!(len<0?(node->text = limDup(string)):(node->text = limSub(string, len)))) {
            if (node->brackets[0]) free(node->brackets[0]);
            if (node->brackets[1]) free(node->brackets[1]);
            free(node);
            return NULL;
        }
    }
    else node->text = NULL;
    
    return node;
}

void limPrint(lim_s **t, int margin){
    int i;
    for (i=0; t[i]; ++i) {
        for (int j=0; j<margin; ++j) fputs(" ", stdout);
        if (t[i]->text) printf("\"%s\"\n", t[i]->text);
        else {
            puts(t[i]->brackets[0]?t[i]->brackets[0]:"error:none");
            if (t[i]->list) limPrint(t[i]->list, margin+2);
            for (int j=0; j<margin; ++j) fputs(" ", stdout);
            puts(t[i]->brackets[1]?t[i]->brackets[1]:"error:none");
        }
    }
    
}

lim_s** limParseR(char *s, char *closing, char **pair_o, char **pair_c, char **singletons, int *cur){
    
    #define m_final (llen-1)
    #define stretchNodeList {\
        ++llen;\
        if (!(tlist = (lim_s **) realloc(list, (llen+1)*sizeof(lim_s *)))) {\
            lim_errno = LIM_NOMEM;\
            limFree(list);\
            return NULL;\
        }\
        else {\
            list = tlist;\
            list[llen] = NULL;\
        }\
    }
    #define addNodeToList(NODE) {\
        stretchNodeList;\
        if (!(list[m_final] = (NODE))) {\
            lim_errno = LIM_NOMEM;\
            limFree(list);\
            return NULL;\
        }\
    }
    
    
    // -- logic start --
    
    lim_s** list = NULL, **tlist;   // tlist is used as a backup when reallocing
    int llen = 0;   // length of `list' (without the terminating NULL
    int closingl;   // length of closing bracket.
    int mark = 0;   // to mark the first character os `s' that hasn't been processed yet
    char *op, *cl;  // to store the matching brackets if found
    int lop, lcl;   // lengths of both brackets
    
    if (!cur) cur = (int[]){0};
    
    if (closing) closingl = strlen(closing);
    else closingl = 0;
    0[cur]=0;
    
    // fputs("deeper\n", stderr);
    
    for (cur[0] = 0; s[cur[0]]; ++cur[0]) {
        // fprintf(stderr, " -  %s\n", s+0[cur]); // DEBUG MAGIC FUNCTION
        
        if (closing && !strncmp(closing, s+cur[0], closingl)) { /*fputs("out\n", stderr); */ break; }
        // - searching for an opining bracket
        op = cl = NULL;
        if (pair_o && pair_c) for (int m = 0; pair_o[m]; ++m) if (!strncmp(pair_o[m], s+cur[0], strlen(pair_o[m]))) {
            op = pair_o[m];
            cl = pair_c[m];
            lop = strlen(op);
            lcl = strlen(cl);
            goto DONE;
        }
        if (singletons) for (int m = 0; singletons[m]; ++m) if (!strncmp(singletons[m], s+cur[0], strlen(singletons[m]))) {
            op = singletons[m];
            cl = singletons[m];
            lcl = lop = strlen(op);
            goto DONE;
        }       
        DONE:
        
        // - if a match is found
        if (op) { 
            int sub_cur = 0;
            // -- 
            if (mark != cur[0]) addNodeToList(limNode(NULL, NULL, s+mark, cur[0]-mark));
            
            addNodeToList(limNode(op, cl, NULL, 0));

            if (!(list[m_final]->list = limParseR(s+cur[0]+lop, cl, pair_o, pair_c, singletons, &sub_cur))) {
                lim_errno = __LINE__;
                // if this is the first return, consider the length of the opning bracket
                if (sub_cur) cur[0] += lop;
                cur[0]+=sub_cur;
                // no need for slen since relavent recursive return can only be started by limFindClosing
                limFree(list);
                return NULL;
            }

            // -1 and +1 is to counter the the for loop's incrementation
            cur[0] += (sub_cur + lcl + lop - 1);
            mark = cur[0] + 1;                
            
        }
    
    }
    
    // if a closing bracket was not found, throw up.
    if (closing && strncmp(closing, s+cur[0], closingl)) {
        lim_errno = __LINE__;
        cur[0]=0;
        limFree(list);
        return NULL;
    }
    // if there is still some text that remains unprocessed, process it.
    if (mark != cur[0]) addNodeToList(limNode(NULL, NULL, s+mark, cur[0]-mark));
    // if empty, return a blank.
    if (!llen) addNodeToList(limNode(NULL, NULL, "", -1)); //blank
    return list;
}

lim_s** limParse(char *s, char **pair_o, char **pair_c, char **singletons, int *cur){
    return limParseR(s, NULL, pair_o, pair_c, singletons, cur);
}


#ifdef LIM_EMBEDED_TEST
int main(){
    lim_s **result;
    char *test;
    int counter, e = 0;
    
    #define CHECK_TEXT_NODE(NODE, TEXT)\
        ABORT_IF(           !NODE->text)\
        ABORT_IF(           NODE->list)\
        ABORT_IF(           NODE->brackets[0])\
        ABORT_IF(           NODE->brackets[1])\
        ABORT_IF_MISMATCH(  NODE->text, TEXT)
    
    #define CHECK_LIST_NODE(NODE, COUNT, OP, CL)\
        ABORT_IF(           !NODE->list)\
        ABORT_IF(           NODE->text)\
        ABORT_IF_MISMATCH(  NODE->brackets[0], OP)\
        ABORT_IF_MISMATCH(  NODE->brackets[1], CL)\
        for (counter=0;NODE->list[counter];++counter);\
        if (counter != COUNT) {\
            fprintf (stderr, "%d: expected %d elements but got %d!\n", __LINE__, COUNT, counter);\
            ABORT_IF(1);\
        }
        
    fputs("testing main parser: ", stderr);
    test =  "abc"
            "(---"
                "def"
            "--)"
            "g"
            "(---"
                "start"
                "***"
                    "p"
                    "|"
                        "s"
                    "|"
                    "l"
                "***"
                "end"
                "(---"
                    "jdf"
                "--)"
            "--)"
            "(---"
                "l"
            "--)"
            "(---"
            "--)"
            "{\\{{{}}}}"
            "d"
            "||"
            "ddd"
            "(---"
                "|"
                    "(---"
                        "chicken "
                        "|"
                            "browser"
                        "|"
                        " "
                        "|"
                            "yup"
                        "|"
                        "|"
                            "true"
                        "|"
                    "--)"
                "|"
            "--)";
    ABORT_IF (!(result = limParse(test, (char *[]){"(---", "{", NULL}, (char *[]){"--)", "}", NULL}, 
                        (char *[]){"***", "|",  NULL}, &e)))
    
    //limPrint(result, 4);   
    
    
    CHECK_TEXT_NODE(result[0], "abc");
    CHECK_LIST_NODE(result[1], 1, "(---", "--)")
    CHECK_TEXT_NODE(result[1]->list[0], "def");
    CHECK_TEXT_NODE(result[2], "g");
    CHECK_LIST_NODE(result[3], 4, "(---", "--)")
    CHECK_TEXT_NODE(result[3]->list[0], "start");
    CHECK_LIST_NODE(result[3]->list[1], 3, "***", "***")
    CHECK_TEXT_NODE(result[3]->list[1]->list[0], "p");
    CHECK_LIST_NODE(result[3]->list[1]->list[1], 1, "|", "|")
    CHECK_TEXT_NODE(result[3]->list[1]->list[2], "l");
    CHECK_TEXT_NODE(result[3]->list[2], "end");
    CHECK_LIST_NODE(result[3]->list[3], 1, "(---", "--)")
    CHECK_TEXT_NODE(result[3]->list[3]->list[0], "jdf");
    CHECK_LIST_NODE(result[4], 1, "(---", "--)")
    CHECK_TEXT_NODE(result[4]->list[0], "l");
    CHECK_LIST_NODE(result[5], 1, "(---", "--)")
    CHECK_TEXT_NODE(result[5]->list[0], "");
    CHECK_LIST_NODE(result[6], 2, "{", "}")
    CHECK_TEXT_NODE(result[6]->list[0], "\\");
    CHECK_LIST_NODE(result[6]->list[1], 1, "{", "}")
    CHECK_LIST_NODE(result[6]->list[1]->list[0], 1, "{", "}")
    CHECK_LIST_NODE(result[6]->list[1]->list[0]->list[0], 1, "{", "}")
    CHECK_TEXT_NODE(result[6]->list[1]->list[0]->list[0]->list[0], "");
    CHECK_TEXT_NODE(result[7], "d");
    CHECK_LIST_NODE(result[8], 1, "|", "|")
    CHECK_TEXT_NODE(result[8]->list[0], "");
    CHECK_TEXT_NODE(result[9], "ddd");
    CHECK_LIST_NODE(result[10], 1, "(---", "--)")
    CHECK_LIST_NODE(result[10]->list[0], 1, "|", "|");
    CHECK_LIST_NODE(result[10]->list[0]->list[0], 5, "(---", "--)");
    CHECK_TEXT_NODE(result[10]->list[0]->list[0]->list[0], "chicken ");
    CHECK_LIST_NODE(result[10]->list[0]->list[0]->list[1], 1, "|", "|");
    CHECK_TEXT_NODE(result[10]->list[0]->list[0]->list[1]->list[0], "browser");
    CHECK_TEXT_NODE(result[10]->list[0]->list[0]->list[2], " ");
    CHECK_LIST_NODE(result[10]->list[0]->list[0]->list[3], 1, "|", "|");
    CHECK_TEXT_NODE(result[10]->list[0]->list[0]->list[3]->list[0], "yup");
    CHECK_LIST_NODE(result[10]->list[0]->list[0]->list[4], 1, "|", "|");
    CHECK_TEXT_NODE(result[10]->list[0]->list[0]->list[4]->list[0], "true");
    
    ABORT_IF(e != 125)
    
    limFree(result);
    
    test = "dsa(--dddf-){-- -}{--{--ddfdfg(--sdff(---)(--ddff-)-}dd-}dd";
    //test = "abc(---sdfghju(---ddf-)(---sad(---kitten";
    ABORT_IF ((result = limParse(test, (char *[]){"(--", "{--", NULL}, (char *[]){"-)", "-}", NULL}, NULL, &e)))
    ABORT_IF (e != 30)
    limFree(result);
    
    test = 
    "ألبان{"
        "أجبان{"
            "موزرلا، جبن حسين"
        "}"
        "لبن{"
            "مراعيزو وأنادكا"
        "}"
        "حليب{}"
    "}";
    ABORT_IF (!(result = limParse(test, (char *[]){"{", NULL}, (char *[]){"}", NULL}, NULL, &e)))
    CHECK_TEXT_NODE(result[0], "ألبان");
    CHECK_LIST_NODE(result[1], 6, "{", "}")
    CHECK_TEXT_NODE(result[1]->list[0], "أجبان");
    CHECK_TEXT_NODE(result[1]->list[2], "لبن");
    CHECK_TEXT_NODE(result[1]->list[4], "حليب");
    CHECK_LIST_NODE(result[1]->list[1], 1, "{", "}")
    CHECK_LIST_NODE(result[1]->list[3], 1, "{", "}")
    CHECK_LIST_NODE(result[1]->list[5], 1, "{", "}")
    CHECK_TEXT_NODE(result[1]->list[1]->list[0], "موزرلا، جبن حسين");
    CHECK_TEXT_NODE(result[1]->list[3]->list[0], "مراعيزو وأنادكا");
    CHECK_TEXT_NODE(result[1]->list[5]->list[0], "");
    limFree(result);
    
    fputs(" ok\n", stderr);
    return 0;
}
#endif

