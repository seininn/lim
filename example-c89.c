/*
    compilation process for x89 programs:
    gcc -std=c99 -c lim.c -o lim.o
    gcc -std=c89 -Wall -Wextra example-c89.c lim.o -o example-c89
*/

#include <stdio.h>
#include "lim.h"

int main(int argc, char **argv){
    lim_s **result;
    int cur;
    char *op[] = {"(", "{", NULL};
    char *cl[] = {")", "}", NULL};
    
    
    /* need an argument */
    if (argc < 2) {
        fprintf(stderr, "%s: no string supplied!\n", argv[0]);
        return 1;
    }
    
    /* check if argument is a sane sting, and print an error message otherwise */
    if (!(result = limParse(argv[1], op, cl, NULL, &cur))){
        fprintf(stderr, "%s: missing closing bracket for brace at offset 0x%x: \"%.6s...\"\n", argv[0], cur, argv[1]+cur); 
        return 1;
    } 
    /* at this point, the string has been completely parsed and stored in result */
    else {
        /* accessing the data structure */
        if (result[0]->text) printf("%s: first node is a text node and is: \"%s\"\n", argv[0], result[0]->text);
        else if (result[0]->list[0]->text) printf("%s: first child of the first node is a text node! and is: \"%s\"\n", argv[0], result[0]->list[0]->text);
        
        /* using a convenience function to print the resulting array */
        limPrint(result, 0);
        
        limFree(result);
        return 0;
    }
}
