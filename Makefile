dev.tests: lim.c lim.h
	gcc -DLIM_EMBEDED_TEST -Wall -Wextra -g -std=c99 lim.c -o dev.tests
	
test: dev.tests
	./dev.tests

example89:
	gcc -std=c99 -c lim.c -o lim.o
	gcc -std=c89 -Wall -Wextra example-c89.c lim.o -o example.c89.bin
	@-rm lim.o

example: example.c lim.c lim.h
	gcc -Wall -Wextra -std=c99 lim.c  example.c -o example.bin

debug: dev.tests
	gdb ./dev.tests

memcheck: dev.tests
	valgrind --tool=memcheck ./dev.tests


all: test example example89

clean:
	@-rm *.tests
	@-rm example*.bin
	

